# JavaScript test

## Installing

````
 git clone https://tomasras@bitbucket.org/tomasras/react-test.git
 npm install
````


## Executing program

Running app in development mode automatically starts webpack dev server
````
npm run dev
````
 To build minified version and check webpack bundle report run the following 
````
npm run build
````
Destination files can be found in ./dist directory 


## Authors

Tomas Rastenis



