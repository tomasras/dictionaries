import { connect } from "react-redux"
import { Dispatch } from "redux"

import DictionaryListSideBar from "./Components/DictionaryListSideBar"
import { showAddDictionaryPopup } from "./dictionaryListActions"

const mapStateToProps = (state: IState) => ({
    dictionaryList: state.dictionaryPageReducer.dictionaryList
})

const mapDispatchToProps = (dispatch: Dispatch) => ({
    showAddDictionaryPopup: () => dispatch(showAddDictionaryPopup())
})

export default connect(mapStateToProps, mapDispatchToProps)(DictionaryListSideBar)