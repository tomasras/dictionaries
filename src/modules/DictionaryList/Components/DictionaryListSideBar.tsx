import * as React from "react"
import styled from "styled-components"
import FilterDictionariesForm from "./DictionaryListSideBar/FilterDictionariesForm"
import DictionaryList from "./DictionaryListSideBar/Dictionarylist"
import Button from "../../../Components/Button"

const SideBar = styled.div`
  height: 100vh;
  width: 350px;
  border-right: 1px solid #efefef;
  flex-shrink: 0;
  position: relative;
  overflow-y: auto;
`

const ButtonContainer = styled.div`
  height: 100px;
  display: flex;
  justify-content: center;
  align-items: center;
  border-bottom: 1px solid #efefef;
`

interface IProps {
    showAddDictionaryPopup: () => IReduxAction
    dictionaryList: IDictionary[]
}

const DictionaryListSideBar: React.FunctionComponent<IProps> = ({showAddDictionaryPopup, dictionaryList}) => (
    <SideBar>
        <ButtonContainer>
            <Button
                primary={true}
                onClick={showAddDictionaryPopup}
            >
                ADD NEW DICTIONARY
            </Button>
        </ButtonContainer>
        <FilterDictionariesForm/>
        <DictionaryList dictionaryList={dictionaryList}/>
    </SideBar>
)

export default DictionaryListSideBar