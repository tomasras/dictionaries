import * as React from "react"
import styled from "styled-components"

const Form = styled.form`
  height: 70px;
  border-bottom: 1px solid #666666;
  position: relative;
`

const Input = styled.input`
  height: 100%;
  width: 100%;
  border: none;
  padding: 30px;
  font-size: 18px;
`
const FilterIcon = styled.i`
  position: absolute;
  right: 30px;
  top: 50%;
  transform: translateY(-50%);
  color: #666666;
`

// @todo implement filter functionality

const FilterDictionariesForm = () => (
    <Form>
        <Input disabled={true} placeholder="Filter dictionaries" type="text"/>
        <FilterIcon className="fas fa-filter"/>
    </Form>
)

export default FilterDictionariesForm