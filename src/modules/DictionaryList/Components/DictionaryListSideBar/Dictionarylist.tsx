import * as React from "react"
import styled from "styled-components"

import DictionaryListitem from "./DictionaryListItem"

const List = styled.ul`
  list-style: none;
  padding: 0;
  margin: 0;
`

interface IProps {
    dictionaryList: IDictionary[]
}

const DictionaryList: React.FunctionComponent<IProps> = ({dictionaryList}) => (
    <List>

        {dictionaryList.map((dictionary: IDictionary) => <DictionaryListitem
                key={dictionary.id}
                name={dictionary.name}
            />
        )}
    </List>
)

export default DictionaryList