import * as React from "react"
import styled from "styled-components"

const ListItem = styled.li`
  border-bottom: 1px solid #efefef;
  transition: all .5s ease;
  width: 100%;
  background-color: white;
  
   &:hover {
      background-color: #ececff;
   }
`

const ListItemInner = styled.div`
  padding: 20px 30px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  cursor: pointer;
  color: #666666;
  
  &.active {
    background-color: #ececff;

     i {
       transform: translateX(20px);
     }
  }
`

interface IProps {
    name: string
}

const DictionaryListItem: React.FunctionComponent<IProps> = ({name}) => (
        <ListItem>
            <ListItemInner>
                <span>{name}</span>
                <i className="fas fa-angle-double-right"/>
            </ListItemInner>
        </ListItem>
)

export default DictionaryListItem