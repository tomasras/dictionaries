const constants = {
    SHOW_ADD_DICTIONARY_POPUP: "SHOW_ADD_DICTIONARY_POPUP"
}

const showAddDictionaryPopup = () => ({
    type: constants.SHOW_ADD_DICTIONARY_POPUP,
})

export {
    constants,
    showAddDictionaryPopup,
}
