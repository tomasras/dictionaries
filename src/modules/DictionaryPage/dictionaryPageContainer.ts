import { connect } from "react-redux"
import { Dispatch } from "redux"

import DictionariesPage from "./Components/DictionaryPage"
import {
    addDictionary,
    hideAddDictionaryPopup,
    removeDictionary,
    addDataset,
    editDataSetRange,
    editDataSetDomain,
    removeDataSet,
    validateAndSaveDataSet,
    editDataSet
} from "./dictionaryPageActions"

interface IDispatchToProps {
    addDictionary: (id: string, name: string) => IReduxAction
    removeDictionary: (id: string) => IReduxAction
    hideAddDictionaryPopup: () => IReduxAction
    addDataSet: (id: string, dictionaryID: string) => IReduxAction
    editDataSetDomain: (id: string, dictionaryID: string, value: string) => IReduxAction
    editDataSetRange: (id: string, dictionaryID: string, value: string) => IReduxAction
    removeDataSet: (id: string, dictionaryID: string) => IReduxAction
    validateAndSaveDataSet: (id: string, dictionaryID: string) => IReduxAction
    editDataSet: (id: string, dictionaryID: string) => IReduxAction
}

interface IStateToProps {
    dictionaryList: IDictionary[]
    showAddDictionaryPopup: boolean
}

const mapStateToProps = (state: IState) => ({
    dictionaryList: state.dictionaryPageReducer.dictionaryList,
    showAddDictionaryPopup: state.dictionaryPageReducer.showAddDictionaryPopup
})

const mapDispatchToProps = (dispatch: Dispatch) => ({
    addDictionary: (id: string, name: string) => dispatch(addDictionary(id, name)),
    removeDictionary: (id: string) => dispatch(removeDictionary(id)),
    hideAddDictionaryPopup: () => dispatch(hideAddDictionaryPopup()),
    addDataSet: (id: string, dictionaryID: string) => dispatch(addDataset(id, dictionaryID)),
    validateAndSaveDataSet: (id: string, dictionaryID: string) => dispatch(validateAndSaveDataSet(id, dictionaryID)),
    editDataSet: (id: string, dictionaryID: string) => dispatch(editDataSet(id, dictionaryID)),
    removeDataSet: (id: string, dictionaryID: string) => dispatch(removeDataSet(id, dictionaryID)),
    editDataSetDomain: (id: string, dictionaryID: string, value: string) =>
        dispatch(editDataSetDomain(id, dictionaryID, value)),
    editDataSetRange: (id: string, dictionaryID: string, value: string) =>
        dispatch(editDataSetRange(id, dictionaryID, value))
})

export default connect<IStateToProps, IDispatchToProps>(mapStateToProps, mapDispatchToProps)(DictionariesPage)
