import { constants as listConst } from "../DictionaryList/dictionaryListActions"
import { constants as pageConst } from "./dictionaryPageActions"

const initialState: IDictionaryList = {
    showAddDictionaryPopup: false,
    dictionaryList: []
}

export default function ReceiptTrackerReducer(state = initialState, action: IReduxAction): IDictionaryList {

    switch (action.type) {

        case listConst.SHOW_ADD_DICTIONARY_POPUP:
            return {
                ...state,
                showAddDictionaryPopup: true
            }

        case pageConst.HIDE_ADD_DICTIONARY_POPUP:
            return {
                ...state,
                showAddDictionaryPopup: false
            }

        case pageConst.ADD_DICTIONARY:
            return {
                ...state,
                showAddDictionaryPopup: false,
                dictionaryList: [
                    ...state.dictionaryList,
                    action.data
                ]
            }

        case pageConst.REMOVE_DICTIONARY:
            return {
                ...state,
                dictionaryList: state.dictionaryList.filter((dictionary: IDictionary) => dictionary.id !== action.data)
            }

        case pageConst.ADD_DATA_SET:
            return {
                ...state,
                dictionaryList: state.dictionaryList.map((dictionary: IDictionary) => {
                    if (dictionary.id === action.data.dictionaryID) {
                        const dataSets = !!dictionary.dataSets ? dictionary.dataSets : []
                        return {
                            ...dictionary,
                            isEditing: true,
                            dataSets: [
                                ...dataSets,
                                {
                                    id: action.data.id,
                                    isEditing: true
                                }
                            ]
                        }
                    }
                    return dictionary
                })
            }

        case pageConst.REMOVE_DATA_SET:
            return {
                ...state,
                dictionaryList: state.dictionaryList.map((dictionary: IDictionary) => {
                    if (dictionary.id === action.data.dictionaryID) {
                        return {
                            ...dictionary,
                            dataSets: dictionary.dataSets.filter((dataSet: IDataSet) => dataSet.id !== action.data.id)
                        }
                    }
                    return dictionary
                })
            }

        case pageConst.EDIT_DATA_SET_DOMAIN:
            return {
                ...state,
                dictionaryList: updateDataSetCell(state.dictionaryList, action, true)
            }

        case pageConst.EDIT_DATA_SET_RANGE:
            return {
                ...state,
                dictionaryList: updateDataSetCell(state.dictionaryList, action, false)
            }

        case pageConst.EDIT_DATA_SET:
            return {
                ...state,
                dictionaryList: changeEditingState(state.dictionaryList, action, true)
            }

        case pageConst.SAVE_DATA_SET:
            return {
                ...state,
                dictionaryList: changeEditingState(state.dictionaryList, action, false)
            }

        case pageConst.DISPATCH_ERROR_MESSAGE:
            return {
                ...state,
                dictionaryList: state.dictionaryList.map((dictionary: IDictionary) => {
                    if (dictionary.id === action.data.dictionaryID) {
                        return {
                            ...dictionary,
                            hasError: action.data.errorCode > 2,
                            dataSets: dictionary.dataSets.map((dataSet: IDataSet) => {
                                if (dataSet.id === action.data.id) {
                                    return addErrors(dataSet, action)
                                }
                                return dataSet
                            })
                        }
                    }
                    return dictionary
                })
            }

        case pageConst.CLEAR_ERRORS:
            return {
                ...state,
                dictionaryList: state.dictionaryList.map((dictionary: IDictionary) => {
                    if (dictionary.id === action.data.dictionaryID) {
                        return {
                            ...dictionary,
                            hasError: !(action.data.errorCode > 2),
                            dataSets: dictionary.dataSets
                                .map((dataSet: IDataSet) => ({
                                    ...dataSet,
                                    errors: clearDataSetErrors(dataSet, action)
                                }))
                        }
                    }
                    return dictionary
                })
            }

        default:
            return state
    }
}

const clearDataSetErrors = (dataSet: IDataSet, action: IReduxAction) => {
    const errors = !!dataSet.errors ? dataSet.errors : []
    return errors.filter((error) => error.errorCode !== action.data.errorCode)
}

const addErrors = (dataSet: IDataSet, action: IReduxAction) => {
    const errors = !!dataSet.errors ? dataSet.errors : []

    if (errors.filter((error: IError) => error.message === action.data.message).length) {
        return dataSet
    }

    return {
        ...dataSet,
        hasError: true,
        isEditing: true,
        errors: [
            ...errors,
            {
                message: action.data.message,
                errorCode: action.data.errorCode
            }
        ]
    }
}

const changeEditingState = (dictionaryList: IDictionary[], action: IReduxAction, editing: boolean) => {
    return dictionaryList.map((dictionary: IDictionary) => {
        if (dictionary.id === action.data.dictionaryID) {
            return {
                ...dictionary,
                isEditing: editing,
                dataSets: dictionary.dataSets.map((dataSet: IDataSet) => {
                    if (dataSet.id === action.data.id) {
                        return {
                            ...dataSet,
                            isEditing: editing
                        }
                    }
                    return dataSet
                })
            }
        }
        return dictionary
    })
}

const updateDataSetCell = (dictionaryList: IDictionary[], action: IReduxAction, isDomain: boolean) => {
    const updateKey = isDomain ? "domain" : "range"
    return dictionaryList.map((dictionary: IDictionary) => {
        if (dictionary.id === action.data.dictionaryID) {
            return {
                ...dictionary,
                dataSets: dictionary.dataSets.map((dataSet: IDataSet) => {
                    if (dataSet.id === action.data.id) {
                        const updateVal = action.data.value
                        return {
                            ...dataSet,
                            [updateKey]: updateVal
                        }
                    }
                    return dataSet
                })
            }
        }
        return dictionary
    })
}
