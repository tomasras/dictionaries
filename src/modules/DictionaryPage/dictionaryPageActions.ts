const constants = {
    ADD_DICTIONARY: "ADD_DICTIONARY",
    HIDE_ADD_DICTIONARY_POPUP: "HIDE_ADD_DICTIONARY_POPUP",
    REMOVE_DICTIONARY: "REMOVE_DICTIONARY",
    ADD_DATA_SET: "ADD_DATA_SET",
    EDIT_DATA_SET_DOMAIN: "EDIT_DATA_SET_DOMAIN",
    EDIT_DATA_SET_RANGE: "EDIT_DATA_SET_RANGE",
    REMOVE_DATA_SET: "REMOVE_DATA_SET",
    DISPATCH_ERROR_MESSAGE: "DISPATCH_ERROR_MESSAGE",
    EDIT_DATA_SET: "EDIT_DATA_SET",
    SAVE_DATA_SET: "SAVE_DATA_SET",
    VALIDATE_AND_SAVE_DATA_SET: "VALIDATE_AND_SAVE_DATA_SET",
    CLEAR_ERRORS: "CLEAR_ERRORS"
}

const addDictionary = (id: string, name: string) => ({
    type: constants.ADD_DICTIONARY,
    data: {
        id,
        name
    }
})

const hideAddDictionaryPopup = () => ({
    type: constants.HIDE_ADD_DICTIONARY_POPUP
})

const removeDictionary = (id: string) => ({
    type: constants.REMOVE_DICTIONARY,
    data: id
})

const addDataset = (id: string, dictionaryID: string) => ({
    type: constants.ADD_DATA_SET,
    data: {
        id,
        dictionaryID
    }
})

const validateAndSaveDataSet = (id: string, dictionaryID: string) => ({
    type: constants.VALIDATE_AND_SAVE_DATA_SET,
    data: {
        id,
        dictionaryID
    }
})

const editDataSet = (id: string, dictionaryID: string) => ({
    type: constants.EDIT_DATA_SET,
    data: {
        id,
        dictionaryID
    }
})

const removeDataSet = (id: string, dictionaryID: string) => ({
    type: constants.REMOVE_DATA_SET,
    data: {
        id,
        dictionaryID
    }
})

const editDataSetDomain = (id: string, dictionaryID: string, value: string) => ({
    type: constants.EDIT_DATA_SET_DOMAIN,
    data: {
        id,
        dictionaryID,
        value
    }
})

const editDataSetRange = (id: string, dictionaryID: string, value: string) => ({
    type: constants.EDIT_DATA_SET_RANGE,
    data: {
        id,
        dictionaryID,
        value
    }
})

export {
    constants,
    hideAddDictionaryPopup,
    addDictionary,
    removeDictionary,
    addDataset,
    removeDataSet,
    editDataSetDomain,
    editDataSetRange,
    validateAndSaveDataSet,
    editDataSet
}
