import * as React from "react"
import styled from "styled-components"

import Dictionary from "./DictionaryPage/Dictionary"
import Popup from "./DictionaryPage/Popup"
import Overlay from "../../../Components/Overlay"

const Container = styled.div`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
  align-items: flex-start;
  align-content: flex-start;
  background-color: #C6CCF8;
  flex-grow: 1;
  min-height: calc(100vh - 30px);
  padding: 30px 50px;
`

interface IProps {
    showAddDictionaryPopup: boolean
    hideAddDictionaryPopup: () => IReduxAction
    addDictionary: (id: string, name: string) => IReduxAction
    addDataSet: (id: string, dictionaryID: string) => IReduxAction
    removeDictionary: (id: string) => IReduxAction
    dictionaryList: IDictionary[]
    editDataSetDomain: (id: string, dictionaryID: string, value: string) => IReduxAction
    editDataSetRange: (id: string, dictionaryID: string, value: string) => IReduxAction
    removeDataSet: (id: string, dictionaryID: string) => IReduxAction
    validateAndSaveDataSet: (id: string, dictionaryID: string) => IReduxAction
    editDataSet: (id: string, dictionaryID: string) => IReduxAction
}

const DictionaryPage: React.FunctionComponent<IProps> = ({
                                                             showAddDictionaryPopup,
                                                             hideAddDictionaryPopup,
                                                             addDictionary,
                                                             dictionaryList,
                                                             removeDictionary,
                                                             addDataSet,
                                                             removeDataSet,
                                                             editDataSetDomain,
                                                             editDataSetRange,
                                                             validateAndSaveDataSet,
                                                             editDataSet
                                                         }) => (
    <React.Fragment>
        {showAddDictionaryPopup && <Popup addDictionary={addDictionary}/>}
        {showAddDictionaryPopup && <Overlay
            onClick={hideAddDictionaryPopup}
        />}
        <Container>
            {dictionaryList.map((dictionary: IDictionary) =>
                <Dictionary
                    key={dictionary.id}
                    {...dictionary}
                    removeDictionary={removeDictionary}
                    addDataSet={addDataSet}
                    removeDataSet={removeDataSet}
                    editDataSetDomain={editDataSetDomain}
                    editDataSetRange={editDataSetRange}
                    validateAndSaveDataSet={validateAndSaveDataSet}
                    editDataSet={editDataSet}
                />
            )}
        </Container>
    </React.Fragment>
)

export default DictionaryPage