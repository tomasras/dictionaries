import * as React from "react"
import styled, { keyframes } from "styled-components"
import * as uuid from "uuid/v1"
import { cold } from "react-hot-loader"

import Button from "../../../../Components/Button"

const bounce = keyframes`
 0%, 100%, 20%, 50%, 80% {
        transform:         translateX(0)
    }
    40% {
        transform:         translateX(-15px)
    }
    60% {
        transform:         translateX(-7.5px)
    }
`

const Form = styled.form`
  border-radius: 7px;
  z-index: 2;
  padding: 20px;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: white;
  position: fixed;
  justify-items: center;
  top: 50%;
  left: 50%;
  min-width: 300px;
  min-height: 150px;
  transform: translate(-50%, -50%);
  flex-direction: column;
   input {
     margin-bottom: 10px;
     width: 100%;
     padding: 10px;
     
     &.error {
     animation: ${bounce} .6s ease-in-out both;
     }
   }
   button {
     justify-self: flex-end;
   }  
`

interface IProps {
    addDictionary: (id: string, name: string) => IReduxAction
}

const Popup: React.FunctionComponent<IProps> = ({addDictionary}) => {

    const [error, setError] = React.useState("")

    const formSubmit = (e: React.FormEvent) => {
        e.preventDefault()
        setError("")

        const form = e.target as HTMLFormElement
        const name = new FormData(form).get("name").toString().trim()

        if (!name.length) {
            setError("error")
            setTimeout(() => setError(""), 600)
            return
        }

        addDictionary(uuid(), name)
    }

    return (
        <Form onSubmit={formSubmit}>
            <input
                onFocus={() => setError("")}
                type="text"
                name="name"
                placeholder="Enter dictionary name"
                className={error}
            />
            <Button primary={true}>ADD</Button>
        </Form>
    )
}

export default cold(Popup)