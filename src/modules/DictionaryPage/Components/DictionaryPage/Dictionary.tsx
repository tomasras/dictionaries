import * as React from "react"
import styled from "styled-components"

import DictionaryNameRow from "./Dictionary/DictionaryNameRow"
import DataSet from "./Dictionary/DictionaryDataSet"
import AddDataSetRow from "./Dictionary/AddDataSetRow"
import Button from "../../../../Components/Button"
import Row from "../../../../Components/Table/Row"
import Cell from "../../../../Components/Table/Cell"

const Container = styled.div`
  min-width: 40%;
  border-radius: 5px;
  margin: 25px 10px;
  align-self: flex-start;
`

const CloseIcon = styled.i`
  color: white;
`

interface IProps {
    id: string
    name: string
    removeDictionary: (id: string) => IReduxAction
    addDataSet: (id: string, dictionaryID: string) => IReduxAction
    dataSets: IDataSet[]
    editDataSetDomain: (id: string, dictionaryID: string, value: string) => IReduxAction
    editDataSetRange: (id: string, dictionaryID: string, value: string) => IReduxAction
    removeDataSet: (id: string, dictionaryID: string) => IReduxAction
    validateAndSaveDataSet: (id: string, dictionaryID: string) => IReduxAction
    editDataSet: (id: string, dictionaryID: string) => IReduxAction
    hasError: boolean
}

const Dictionary: React.FunctionComponent<IProps> = ({
                                                         name,
                                                         id,
                                                         removeDictionary,
                                                         addDataSet,
                                                         dataSets,
                                                         removeDataSet,
                                                         editDataSetRange,
                                                         editDataSetDomain,
                                                         validateAndSaveDataSet,
                                                         editDataSet,
                                                         hasError
                                                     }) => (
    <Container>
        <Row header>
            <Cell>Domain</Cell>
            <Cell>Range</Cell>
            <Button
                onClick={() => removeDictionary(id)}
                style={{
                    top: "50%"
                }}
            >
                <CloseIcon className="far fa-times-circle"/>
            </Button>
        </Row>
        <DictionaryNameRow name={name}/>

        {!!dataSets ? dataSets.map((dataSet: IDataSet) =>
                <DataSet
                    validateAndSaveDataSet={validateAndSaveDataSet}
                    editDataSet={editDataSet}
                    key={dataSet.id}
                    removeDataSet={removeDataSet}
                    editDataSetRange={editDataSetRange}
                    editDataSetDomain={editDataSetDomain}
                    dictionaryID={id}
                    {...dataSet}
                />
            ) :
            null}

        <AddDataSetRow
            addDataSet={addDataSet}
            dictionaryID={id}
            hasError={hasError}
        />
    </Container>
)

export default Dictionary