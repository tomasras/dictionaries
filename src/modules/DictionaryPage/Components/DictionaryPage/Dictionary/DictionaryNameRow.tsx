import * as React from "react"

import Button from "../../../../../Components/Button"
import Row from "../../../../../Components/Table/Row"
import Cell from "../../../../../Components/Table/Cell"

interface IProps {
    name: string
}

// @todo implement edit name functionality

const DictionaryNameRow: React.FunctionComponent<IProps> = ({name}) => (
    <Row highlighted>
        <Cell>{name}</Cell>
        <Cell>
            {/* <Button>
                <i className="far fa-edit"/>
            </Button>*/}
        </Cell>
    </Row>
)

export default DictionaryNameRow