import * as React from "react"
import * as uuid from "uuid/v1"
import { cold } from "react-hot-loader"

import Button from "../../../../../Components/Button"
import Row from "../../../../../Components/Table/Row"
import Cell from "../../../../../Components/Table/Cell"
import ErrorMsg from "../../../../../Components/Table/ErrorMsg"

interface IProps {
    dictionaryID: string,
    addDataSet: (id: string, dictionaryID: string) => IReduxAction
    hasError: boolean
}

const AddDataSetRow: React.FunctionComponent<IProps> = ({
                                                            addDataSet,
                                                            dictionaryID,
                                                            hasError
                                                        }) => {

    const [displayError, setErrorMsgState] = React.useState(false)

    return (
        <React.Fragment>
            {(displayError && hasError) &&
            <ErrorMsg code={3}>Please correct errors above</ErrorMsg>
            }
            <Row highlighted>
                <Cell>Add new data set</Cell>
                <Cell>
                    <Button
                        onClick={() => {
                            if (!hasError) {
                                addDataSet(uuid(), dictionaryID)
                            } else {
                                setErrorMsgState(true)
                                setTimeout(() => setErrorMsgState(false), 3000)
                            }
                        }}
                    >
                        <i className="fas fa-plus"/>
                    </Button>
                </Cell>
            </Row>
        </React.Fragment>
    )
}

export default cold(AddDataSetRow)
