import * as React from "react"
import styled from "styled-components"

import Row from "../../../../../Components/Table/Row"
import Cell from "../../../../../Components/Table/Cell"
import ErrorMsg from "../../../../../Components/Table/ErrorMsg"
import Button from "../../../../../Components/Button"

const Input = styled.input`
  width: 100%;
`

interface IProps {
    editDataSetDomain: (id: string, dictionaryID: string, value: string) => IReduxAction
    editDataSetRange: (id: string, dictionaryID: string, value: string) => IReduxAction
    removeDataSet: (id: string, dictionaryID: string) => IReduxAction
    dictionaryID: string,
    id: string,
    range?: string,
    domain?: string,
    validateAndSaveDataSet: (id: string, dictionaryID: string) => IReduxAction
    editDataSet: (id: string, dictionaryID: string) => IReduxAction
    isEditing?: boolean,
    errors?: IError[]
}

const DictionaryDataSet: React.FunctionComponent<IProps> = ({
                                                                removeDataSet,
                                                                editDataSetDomain,
                                                                editDataSetRange,
                                                                dictionaryID,
                                                                id,
                                                                domain,
                                                                range,
                                                                isEditing,
                                                                validateAndSaveDataSet,
                                                                editDataSet,
                                                                errors
                                                            }) => {

    return (
        <React.Fragment>
            {
                errors.map((error: IError, i: number) =>
                    <ErrorMsg
                        key={`${error.message}_${i}`}
                        code={error.errorCode}
                    >
                        {error.message}
                    </ErrorMsg>
                )
            }
            <Row>
                <Cell>
                    <Input
                        style={isEditing ? {border: "1px solid #efefef"} : {border: "none"}}
                        autoFocus={isEditing}
                        readOnly={!isEditing}
                        type="text"
                        value={domain}
                        onChange={(e: React.FormEvent) => {
                            const inputElem = e.target as HTMLInputElement
                            editDataSetDomain(
                                id,
                                dictionaryID,
                                inputElem.value.replace(/\s\s+/g, " "
                                )
                            )
                        }}
                    />
                </Cell>
                <Cell>
                    <Input
                        style={isEditing ? {border: "1px solid #efefef"} : {border: "none"}}
                        readOnly={!isEditing}
                        type="text"
                        value={range}
                        onChange={(e: React.FormEvent) => {
                            const inputElem = e.target as HTMLInputElement
                            editDataSetRange(
                                id,
                                dictionaryID,
                                inputElem.value.replace(/\s\s+/g, " "
                                )
                            )
                        }}
                    />
                </Cell>
                <div>
                    <Button
                        onClick={() => {
                            if (isEditing) {
                                validateAndSaveDataSet(id, dictionaryID)
                            } else {
                                editDataSet(id, dictionaryID)
                            }
                        }}
                    >
                        {isEditing ? <i className="far fa-save"/> : <i className="far fa-edit"/>}
                    </Button>
                    <Button
                        onClick={() => {
                            removeDataSet(id, dictionaryID)
                            validateAndSaveDataSet(id, dictionaryID)
                        }}
                    >
                        <i className="far fa-times-circle"/>
                    </Button>
                </div>
            </Row>
        </React.Fragment>
    )
}

DictionaryDataSet.defaultProps = {
    domain: "",
    range: "",
    errors: []
}

export default DictionaryDataSet