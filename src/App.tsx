import * as React from "react"
import styled, { createGlobalStyle } from "styled-components"
import DictionaryListSideBar from "./modules/DictionaryList/dictionaryListContainer"
import DictionaryPage from "./modules/DictionaryPage/dictionaryPageContainer"

const GlobalStyle = createGlobalStyle`
  * {
    box-sizing: border-box;
  }
  
  html, body {
    margin: 0;
    padding: 0;
    width: 100%;
    font-size: 16px;
    font-family: 'Montserrat', sans-serif;
    color: #666666;
  }
`

const Container = styled.div`
    display: flex;
`

const App = () => (
    <Container>
        <GlobalStyle/>
        <DictionaryListSideBar/>
        <DictionaryPage/>
    </Container>
)

export default App