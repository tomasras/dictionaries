import {Dispatch, Middleware, Store} from "redux"
import {constants as dictionaryPageConst} from "../modules/DictionaryPage/dictionaryPageActions"

export const dictionaryValidationMiddleware:
    Middleware = (store: Store) => (next: Dispatch) => (action: IReduxAction) => {

    if (action.type === dictionaryPageConst.VALIDATE_AND_SAVE_DATA_SET) {
        const dictionaryList = store.getState().dictionaryPageReducer.dictionaryList
        const currDictionary = getDictionary(dictionaryList, action)
        const dataSets = currDictionary.dataSets

        const duplicateDomainRange = validateDuplicateDomainsRanges(dataSets)

        if (duplicateDomainRange.length) {
            duplicateDomainRange.forEach((dataSet: IDataSet) =>
                store.dispatch({
                    type: dictionaryPageConst.DISPATCH_ERROR_MESSAGE,
                    data: {
                        id: dataSet.id,
                        dictionaryID: action.data.dictionaryID,
                        message: "Duplicate domain and range entries",
                        errorCode: 1
                    }
                })
            )
        } else {
            dispatchClearErrors(1, action.data.dictionaryID, store)
        }

        const duplicateDomains = validateDuplicateDomains(dataSets)

        if (duplicateDomains.length) {
            duplicateDomains.forEach((dataSet: IDataSet) =>
                store.dispatch({
                    type: dictionaryPageConst.DISPATCH_ERROR_MESSAGE,
                    data: {
                        id: dataSet.id,
                        dictionaryID: action.data.dictionaryID,
                        message: "Duplicate domain entries",
                        errorCode: 2
                    }
                })
            )
        } else {
            dispatchClearErrors(2, action.data.dictionaryID, store)
        }

        const cycles = validateCycles(dataSets)

        if (cycles.length) {
            cycles.forEach((dataSet: IDataSet) =>
                store.dispatch({
                    type: dictionaryPageConst.DISPATCH_ERROR_MESSAGE,
                    data: {
                        id: dataSet.id,
                        dictionaryID: action.data.dictionaryID,
                        message: "Cycles error",
                        errorCode: 3
                    }
                })
            )
        } else {
            dispatchClearErrors(3, action.data.dictionaryID, store)
        }

        const chains = validateChains(dataSets)

        if (chains.length) {
            chains.forEach((dataSet: IDataSet) =>
                store.dispatch({
                    type: dictionaryPageConst.DISPATCH_ERROR_MESSAGE,
                    data: {
                        id: dataSet.id,
                        dictionaryID: action.data.dictionaryID,
                        message: "Chains error",
                        errorCode: 4
                    }
                })
            )
        } else {
            dispatchClearErrors(4, action.data.dictionaryID, store)
        }

        if (!cycles.length && !chains.length) {
            store.dispatch({
                type: dictionaryPageConst.SAVE_DATA_SET,
                data: {
                    id: action.data.id,
                    dictionaryID: action.data.dictionaryID
                }
            })
        }
    }

    return next(action)
}

const dispatchClearErrors = (code: number, id: string, store: Store) =>
    store.dispatch({
        type: dictionaryPageConst.CLEAR_ERRORS,
        data: {
            dictionaryID: id,
            errorCode: code
        }
    })

const validateDuplicateDomainsRanges = (dataSets: IDataSet[]) => {
    // @ts-ignore
    return dataSets.flatMap((dataSet: IDataSet) =>
        dataSets.filter((filteredSet: IDataSet) =>
            (format(dataSet.range) === format(filteredSet.range)) &&
            (format(dataSet.domain) === format(filteredSet.domain)) &&
            (dataSet.id !== filteredSet.id)))
}

const validateDuplicateDomains = (dataSets: IDataSet[]) => {
    // @ts-ignore
    return dataSets.flatMap((dataSet: IDataSet) =>
        dataSets.filter((filteredSet: IDataSet) =>
            (format(dataSet.range) !== format(filteredSet.range)) &&
            (format(dataSet.domain) === format(filteredSet.domain)) &&
            (dataSet.id !== filteredSet.id)))
}

const validateCycles = (dataSets: IDataSet[]) => {
    // @ts-ignore
    return dataSets.flatMap((dataSet: IDataSet) =>
        dataSets.filter((filteredSet: IDataSet) =>
            (format(dataSet.range) === format(filteredSet.domain)) &&
            (format(dataSet.domain) === format(filteredSet.range)) &&
            (dataSet.id !== filteredSet.id)))
}

const validateChains = (dataSets: IDataSet[]) => {
    // @ts-ignore
    return dataSets.flatMap((dataSet: IDataSet) =>
        dataSets.filter((filteredSet: IDataSet) =>
            ((format(dataSet.domain) === format(filteredSet.range)) ||
                (format(dataSet.range) === format(filteredSet.domain))) &&
            (dataSet.id !== filteredSet.id)))
}

const getDictionary = (dictionaryList: IDictionary[], action: IReduxAction): IDictionary =>
    dictionaryList.filter((dictionary: IDictionary) => dictionary.id === action.data.dictionaryID)[0]

const format = (value: string = "") => value.trim().toLocaleLowerCase()