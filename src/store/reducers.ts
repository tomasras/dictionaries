import { combineReducers } from "redux"

import dictionaryPageReducer from "../modules/DictionaryPage/dictionaryPageReducer"

const rootReducer = combineReducers({
    dictionaryPageReducer
})

export default rootReducer
