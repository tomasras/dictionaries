import { createStore, applyMiddleware, compose } from "redux"
import rootReducer from "./reducers"
import { dictionaryValidationMiddleware } from "./middleware"

declare const __DEV__: boolean

const storeEnhancers: any = []
const middleware: any = [
    dictionaryValidationMiddleware
]

storeEnhancers.push(applyMiddleware(...middleware))

if (__DEV__) {
    const debugEnhancer = (window as any).__REDUX_DEVTOOLS_EXTENSION__ && (window as any).__REDUX_DEVTOOLS_EXTENSION__()
    storeEnhancers.push(debugEnhancer)
}

export default function configureStore(initialState: object) {
    const store = createStore(
        rootReducer,
        initialState,
        compose(...storeEnhancers)
    )

    if (__DEV__ && module.hot) {
        module.hot.accept("./reducers", () =>
            store.replaceReducer(require("./reducers").default)
        )
    }
    return store
}
