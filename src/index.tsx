import * as React from "react"
import * as ReactDOM from "react-dom"
import { AppContainer } from "react-hot-loader"
import { Provider } from "react-redux"
import configureStore from "./store/store"
import { loadState, saveState } from "./localStorage/localStorage"

import App from "./App"

const localStorageState = loadState()
const store = configureStore(localStorageState)
const root = document.getElementById("root")

store.subscribe(() => {
    saveState(store.getState())
})

const render = (Component: React.FunctionComponent) => {
    ReactDOM.render(
        <Provider store={store}>
            <AppContainer>
                <Component/>
            </AppContainer>
        </Provider>,
        root
    )
}

if (module.hot) {
    module.hot.accept("./App", () => {
        const newApp = require("./App").default
        render(newApp)
    })
}

render(App)
