interface IReduxAction {
    type: string
    data?: any
}

interface IState {
    dictionaryPageReducer: IDictionaryList
}

interface IDictionaryList {
    showAddDictionaryPopup: boolean
    dictionaryList: IDictionary[]
}

interface IError {
    message: string,
    errorCode: number
}

interface IDataSet {
    id: string
    domain?: string
    range?: string
    errors?: IError[]
    isEditing?: boolean
}

interface IDictionary {
    name: string
    id: string
    dataSets: IDataSet[]
    hasError: boolean
}
