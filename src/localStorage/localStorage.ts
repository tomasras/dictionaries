const loadState = (): undefined | object => {
    try {
        const serializedSAtate = localStorage.getItem('state');
        if (serializedSAtate === null) {
            return undefined;
        }
        return JSON.parse(serializedSAtate);
    } catch (e) {
        return undefined;
    }
};

const saveState = (state: object): void => {
    try {
        const serializedState = JSON.stringify(state);
        localStorage.setItem('state', serializedState);
    } catch (e) {
        console.error(e); // tslint:disable-line no-console
    }
};

export {
    loadState,
    saveState,
};