import * as React from "react"
import styled from "styled-components"

interface IProps {
    primary?: boolean
}

const StyledButton = styled.button`
  transition: all .3s ease-in-out;
  border-radius: 3px;
  cursor: pointer;
  background: ${(props: IProps) => props.primary ? "#151515a6" : "transparent"};
  color: ${(props: IProps) => props.primary ? "white" : "#666666"};  
  padding: ${(props: IProps) => props.primary ? "15px 20px" : "5px"};
  border: ${(props: IProps) => props.primary ? "1px solid #151515a6" : "transparent"};
  
  ${(props: IProps) => props.primary ? "&:hover { opacity: .5}" : ""}
`
export default StyledButton