import * as React from "react"
import styled from "styled-components"

interface IProps {
    highlighted?: boolean
    header?: boolean
}

const Row = styled.div`
  display: flex;
  justify-content: space-between;
  position: relative;
  align-items: center;
  padding: 0 10px;
  background: white;
  border-bottom: 1px solid #f2f2f2;
  color: #666666;  
  ${(props: IProps) => props.highlighted ? "font-weight: 500; border-color: #7f7f7f;" : ""}
  ${(props: IProps) => props.header ? 
    "background: #6c7ae0; color: #fff; font-size: 18px;  border-radius: 7px 7px 0 0; font-weight: 500;" : ""}
  &:last-child {
    border: none;
    border-radius: 0 0 7px 7px;
  }

`

export default Row