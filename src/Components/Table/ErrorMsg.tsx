import * as React from "react"
import styled from "styled-components"

interface IProps {
    code: number
}

const Row = styled.div`
  padding: 5px 30px;
  font-size: 14px;
  color: red;
  background: white;
  ${(props: IProps) => props.code > 2 ? "color: red;" : "color: green"}
`

export default Row