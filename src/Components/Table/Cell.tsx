import * as React from "react"
import styled from "styled-components"

interface IProps {
    highlighted?: boolean
    header?: boolean
}

const Cell = styled.div`
  display: table-cell;
  padding: 10px 20px;
`

export default Cell