import * as React from "react"
import styled from "styled-components"

const Overlay = styled.div`
  background-color: #CAC2FA;
  position: fixed;
  width: 100vw;
  height: 100vh;
  opacity: .7;
`
export default Overlay