const webpack = require('webpack');
const path = require('path');
const merge = require('webpack-merge');
const baseConfig = require('./base.config.js');

module.exports = merge(baseConfig, {
	mode: 'development',
	entry: [
		"webpack-dev-server/client?http://localhost:8181",
		"webpack/hot/only-dev-server",
		path.resolve(__dirname, "../src/index.tsx")
	],
	devtool: "source-map",
	devServer: {
    host: 'localhost',
    hot: true,
		port: 8181,
    open: true
	},
	plugins: [
		new webpack.HotModuleReplacementPlugin(),
		new webpack.NamedModulesPlugin(),
		new webpack.DefinePlugin({
			'process.env.NODE_ENV': JSON.stringify("development"),
      '__DEV__': true
		})
	]
});
