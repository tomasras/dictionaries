const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const publicDir = path.resolve(__dirname, "../dist/");
const srcDir = path.resolve(__dirname, "../src/");

const entryFile = srcDir + "/index.tsx";

module.exports = {
	entry: entryFile,
	output: {
    filename: '[name].prod.js',
		path: publicDir
	},
	resolve: {
		extensions: [".ts", ".tsx", ".js", ".json", ".jsx", ".scss"]
	},
	module: {
		rules: [
			{
				test: /\.tsx?$/,
				use: [
					{
						loader: "babel-loader",
						options: {
							babelrc: false,
							plugins: [
								'react-hot-loader/babel'
							]
						}
					},
					"awesome-typescript-loader"
				],
				exclude: ["/node_modules/"]
			},
			{
				test: /\.js$/,
				loader: "source-map-loader",
				enforce: "pre"
			},
      {
        test: /\.scss$/,
        loaders: [
          'style-loader',
          { loader: 'css-loader', options: { modules: true } },
          'postcss-loader',
          'sass-loader',
        ],
      },
		]
	},

	plugins: [
    new HtmlWebpackPlugin({template: './src/index.html',}),
	]
};
